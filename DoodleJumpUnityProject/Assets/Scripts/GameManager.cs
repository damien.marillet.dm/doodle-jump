using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    private Camera cam;

    public int score = 0;
    [SerializeField]
    private GameObject gameOverCanvas;
    private bool isGameOverHandled=false;

    public GameObject greenPlatform;
    public GameObject bluePlatform;
    public GameObject brownPlatform;
    private GameObject platform;
    private float greenProba = 0.9f; // probabilty of generating a green platform
    private float blueProba = 1f; // probabilty of generating a blue platform
    private float brownProba = 0.5f; // probabilty of generating a brown platform between two solid platforms (if there is the min space)
    private List<float> platformsProbas;
    private List<GameObject> platformsList;

    public GameObject spring;
    public GameObject trampoline;
    public GameObject cap;
    public GameObject jetpack;
    private GameObject obj;
    private float springProba = 0.6f; 
    private float trampolineProba = 0.1f; 
    private float capProba = 0.2f; 
    private float jetpackProba = 0.1f;
    private float itemDropProba = 0.2f;
    private List<float> itemsProbas;
    private List<GameObject> itemsList;

    public GameObject blackHole;
    private float blackHoleProba = 0.05f;
    public GameObject monsterA;
    public GameObject monsterB;
    private GameObject enemy;
    private float monsterAProba = 0.5f;
    private float monsterBProba = 0.5f;
    private List<float> enemiesProbas;
    private List<GameObject> enemiesList;
    private float enemiesSpawnProba = 0.05f;
    private float altitudeEnemies = 10f;

    private Vector3 lastSolidPlatform;
    private Vector3 lastObjectSpawned;
    private Queue<GameObject> gameObjectsQueue;
    private Vector3 platformsSize;
    private Vector3 spawnPosition;

    private float width;
    private float height;

    private float jumpHeight = 2.8f;

    public bool isGameOver = false;

    // Start is called before the first frame update
    void Start()
    {
        itemsList = new List<GameObject>{spring, trampoline, cap, jetpack};
        itemsProbas = new List<float>{springProba, trampolineProba, capProba, jetpackProba};
        platformsList = new List<GameObject>{bluePlatform};
        platformsProbas = new List<float>{blueProba};
        enemiesList = new List<GameObject>{monsterA, monsterB};
        enemiesProbas = new List<float>{monsterAProba, monsterBProba};

        gameObjectsQueue = new Queue<GameObject>();

        platformsSize = greenPlatform.GetComponent<Renderer>().bounds.size;
        cam = Camera.main.GetComponent<Camera>();
        width = cam.orthographicSize * Screen.width / Screen.height;
        height = cam.orthographicSize;

        // spawning the 1rst platform
        spawnPosition = new Vector3(0f, -height, 0f);
        lastSolidPlatform = spawnPosition;
        lastObjectSpawned = spawnPosition;
        gameObjectsQueue.Enqueue(Instantiate(greenPlatform, spawnPosition, Quaternion.identity));
    }

    void Update()
    {
        // spawn platforms to complete the camera box
        while (lastObjectSpawned.y < cam.transform.position.y + height)
        {
            // spawn monster
            float localEnemiesProba = enemiesSpawnProba;
            float localBlackHoleProba = enemiesSpawnProba;
            if (player.GetComponent<Player>().getHoldingObject()){
                localEnemiesProba = 0f;
                localBlackHoleProba = 0f;
            }
            if (Random.Range(0f, 1f) < localEnemiesProba & player.GetComponent<Rigidbody2D>().position.y > altitudeEnemies){ 
                enemy = chooseFromProba(enemiesList, enemiesProbas);
                spawningSomething(enemy, gameObjectsQueue, false, monsterPosition(enemy));
            // spawn blackHole
            } else if (Random.Range(0f, 1f) < localBlackHoleProba & player.GetComponent<Rigidbody2D>().position.y > altitudeEnemies){
                spawningBlackHole(blackHole, gameObjectsQueue, false, nonSolidPosition(blackHole));
            // spawn brown platform
            } else if (Random.Range(0f, 1f) < brownProba){ 
                spawningSomething(brownPlatform, gameObjectsQueue, false, nonSolidPosition(brownPlatform));
            }
            // spawn solid platform
            spawningPlatform();
        }

        // Destroy game Objects outside camera
        while(gameObjectsQueue.Peek().transform.position.y < cam.transform.position.y - height) // en dehors de la cam
        {
            Destroy(gameObjectsQueue.Dequeue());
        }

        UpdateScore();
        gameOverHandler();
    }


    private float gap(GameObject objectSpawning)
    {
        Vector2 size = objectSpawning.GetComponent<Renderer>().bounds.size;
        return (size.y*0.5f+platformsSize.y*1.5f);
    }

    private float rndX(GameObject objectSpawning)
    {
        Vector2 size = objectSpawning.GetComponent<Renderer>().bounds.size;
        return Random.Range(-width+size.x/2, width-size.x/2);
    }

    private Vector3 monsterPosition(GameObject objectSpawning)
    {
        Vector2 size = objectSpawning.GetComponent<Renderer>().bounds.size;
        // Un monstre ne peut pas spawn au dessus de la derniere platforme solide. -> pondérer les deux range ? 
        float spawnPositionX;
        if (-width+size.x/2 < lastSolidPlatform.x-platformsSize.x/2){
            if (lastSolidPlatform.x+platformsSize.x/2 < width-size.x/2){
                spawnPositionX = Random.Range(1, 3)==1 ? Random.Range(-width+size.x/2, lastSolidPlatform.x-platformsSize.x/2) : Random.Range(lastSolidPlatform.x+platformsSize.x/2,width-size.x/2);
            } else {
                spawnPositionX = Random.Range(-width+size.x/2, lastSolidPlatform.x-platformsSize.x/2);
            }
        } else {
            if (lastSolidPlatform.x+platformsSize.x/2 < width-size.x/2){
                spawnPositionX = Random.Range(lastSolidPlatform.x+platformsSize.x/2,width-size.x/2);
            } else {
                spawnPositionX = 0f;
                print("sould not occure monsterPos");
            }
        }
        return new Vector3(spawnPositionX, spawningDensity(lastSolidPlatform.y+gap(objectSpawning), lastSolidPlatform.y+jumpHeight-gap(objectSpawning)), 0f);
        // return new Vector3(spawnPositionX, Random.Range(lastSolidPlatform.y+gap(objectSpawning), lastSolidPlatform.y+jumpHeight-gap(objectSpawning)), 0f);
    }

    private Vector3 nonSolidPosition(GameObject objectSpawning)
    {
        return new Vector3(rndX(objectSpawning), spawningDensity(lastSolidPlatform.y+gap(objectSpawning), lastSolidPlatform.y+jumpHeight-gap(objectSpawning)), 0f);
        // return new Vector3(rndX(objectSpawning), Random.Range(lastSolidPlatform.y+gap(objectSpawning), lastSolidPlatform.y+jumpHeight-gap(objectSpawning)), 0f);
    }

    private Vector3 solidPosition(GameObject objectSpawning)
    {
        return new Vector3(rndX(objectSpawning), spawningDensity(lastObjectSpawned.y+gap(objectSpawning), lastSolidPlatform.y+jumpHeight), 0f);
        // return new Vector3(rndX(objectSpawning), Random.Range(lastObjectSpawned.y+gap(objectSpawning), lastSolidPlatform.y+jumpHeight), 0f);
    }

    private Vector3 itemPosition(GameObject objectSpawning)
    {
        Vector2 size = objectSpawning.GetComponent<Renderer>().bounds.size;
        return new Vector3(lastSolidPlatform.x, lastSolidPlatform.y+(size.y*0.5f+platformsSize.y*0.2f), 0f);
    }

    private float spawningDensity(float min, float max)
    {
        // float g = Math.abs(Random.NextGaussian());
        float newMin = min-(max-min);
        float g = RandomGaussian(newMin, max);
        if (g<min)
        {
            g = min + (min - g) ;
        }
        return g;
    }

    public static float RandomGaussian(float minValue = 0.0f, float maxValue = 1.0f)
    {
        float u, v, S;

        do
        {
            u = 2.0f * UnityEngine.Random.value - 1.0f;
            v = 2.0f * UnityEngine.Random.value - 1.0f;
            S = u * u + v * v;
        }
        while (S >= 1.0f);

        // Standard Normal Distribution
        float std = u * Mathf.Sqrt(-2.0f * Mathf.Log(S) / S);

        // Normal Distribution centered between the min and max value
        // and clamped following the "three-sigma rule"
        float mean = (minValue + maxValue) / 2.0f;
        float sigma = (maxValue - mean) / 3.0f;
        return Mathf.Clamp(std * sigma + mean, minValue, maxValue);
    }

    private void spawningSomething(GameObject objectSpawning, Queue<GameObject> queue, bool solid, Vector3 position)
    {
        spawnPosition = position;
        queue.Enqueue(Instantiate(objectSpawning, spawnPosition, Quaternion.identity));
        lastObjectSpawned = spawnPosition;
        if (solid){
            lastSolidPlatform = spawnPosition;
        }
    }

    private void spawningBlackHole(GameObject objectSpawning, Queue<GameObject> queue, bool solid, Vector3 position)
    {
        spawnPosition = position;
        GameObject blackHole = Instantiate(objectSpawning, spawnPosition, Quaternion.identity);
        blackHole.GetComponent<BlackHole>().gameManager = this;
        queue.Enqueue(blackHole);
        lastObjectSpawned = spawnPosition;
        if (solid){
            lastSolidPlatform = spawnPosition;
        }
    }

    private void spawningPlatform()
    {   
        float rnd = Random.Range(0f, 1f); // we suppose that sum(listProbas) == 1
        if (rnd < greenProba){ // greenPlatform
            spawningSomething(greenPlatform, gameObjectsQueue, true, solidPosition(greenPlatform));
            // Spawning items
            rnd = Random.Range(0f, 1f);
            if (rnd < itemDropProba)
            {
                // print("drop item");
                GameObject it = chooseFromProba(itemsList, itemsProbas);
                spawningSomething(it, gameObjectsQueue, true, itemPosition(it));
            }
        } else { // other platforms
            platform = chooseFromProba(platformsList, platformsProbas);
            spawningSomething(platform, gameObjectsQueue, true, solidPosition(platform));
        }
    }

    private GameObject chooseFromProba(List<GameObject> listObjects, List<float> listProbas){
        float totProba = 0f;
        float rnd;
        for (int i=0; i<listObjects.Count; i++){
            totProba += listProbas[i];
            rnd = Random.Range(0f, 1f); // we suppose that sum(listProbas) == 1
            if (rnd < totProba){
                return listObjects[i];
            }
        }
        print("CHOOSE NOTHING !!!");
        return null;
    }



    private void gameOverHandler()
    {
        if (!isGameOver || isGameOverHandled) return;
        print("perdu");
        UpdateHighScore();
        gameOverCanvas.SetActive(true);
        isGameOverHandled = true;

    }

    
    private IEnumerator moveUI(GameObject go, float distance, float nbStep)
    {
        Vector3 pos = go.transform.position;
        float step = distance / nbStep;
        while (pos.y<0)
        {
            pos.y += step;
            //pos += screenStep;
            go.transform.position = pos;
            yield return null;
        }
    }
    private void UpdateScore()
    {
        int height = (int)(player.transform.position.y * 100);
        score = score < height ? height : score;
    }
    private void UpdateHighScore()
    {
        if (!PlayerPrefs.HasKey("highScore")) PlayerPrefs.SetInt("highScore", 0);
        if (score > PlayerPrefs.GetInt("highscore"))
        {
            PlayerPrefs.SetInt("highScore", score);
            PlayerPrefs.Save();
        }
    }
    public int GetScore()
    {
        return score;
    }
    public int GetHighScore()
    {
        return PlayerPrefs.GetInt("highScore");
    }
}
