using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButtonScript : MonoBehaviour
{
    private bool isActive = false;
    public void PausePopUpHandler()
    {
        isActive = !isActive;

        for(int i=0; i<transform.childCount;i++)
            transform.GetChild(i).gameObject.SetActive(isActive);

        Time.timeScale = isActive ==true ? 0:1;
    }
}
