using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverCanvasScript : MonoBehaviour
{
    private int score=0;
    private static int highScore;
    [SerializeField]
    private GameManager gmScript;

    public void PlayButton()
    {
        SceneManager.LoadScene("MainScene");
    }
    public void MenuButton ()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void Update()
    {
        if (gmScript.isGameOver)
        {
            score = gmScript.GetScore();
            highScore= gmScript.GetHighScore();
            TextMeshProUGUI scoreTxt = GameObject.Find("ScoreTxt").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI highScoreTxt = GameObject.Find("HighScoreTxt").GetComponent<TextMeshProUGUI>();
            scoreTxt.text = "your score: "+score;
            highScoreTxt.text = "your high score: " + gmScript.GetHighScore();
        }
    }
}

