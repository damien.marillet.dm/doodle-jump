using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSwitch : SwitchButton
{
    private void Awake()
    {
        if (!PlayerPrefs.HasKey("volume"))
            Unmute();
        AudioListener.volume = PlayerPrefs.GetFloat("volume");
        FirstSelected = AudioListener.volume > 0f ? false : true;
    }
    public void Mute()
    {
        setOff();
        PlayerPrefs.SetFloat("volume", 0f);
        AudioListener.volume = PlayerPrefs.GetFloat("volume");
    }
    public void Unmute()
    {
        setOn();
        PlayerPrefs.SetFloat("volume", 1f);
        AudioListener.volume = PlayerPrefs.GetFloat("volume");
    }
    protected override void setOn()
    {
        //call setOn method from parent
        base.setOn();
    }
    protected override void setOff()
    {
        //call setOn method from parent
        base.setOff();
    }
    //Save on Exit
    void OnDisable()
    {
        PlayerPrefs.Save();
    }

}
