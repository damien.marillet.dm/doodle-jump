using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TopBarScript : MonoBehaviour
{
    private TextMeshProUGUI displayText;
    private Camera mainCam;
    [SerializeField]
    private GameManager gmScript;


    // Start is called before the first frame update
    void Start()
    {
        mainCam = Camera.main;
        displayText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        int score = gmScript.GetScore();
        displayText.SetText(score.ToString());
    }

}
