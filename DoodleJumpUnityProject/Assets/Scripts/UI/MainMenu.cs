using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private void Awake()
    {
        if (!PlayerPrefs.HasKey("volume"))
            PlayerPrefs.SetFloat("volume", 1f);
        print("volume:"+PlayerPrefs.GetFloat("volume"));
        AudioListener.volume = PlayerPrefs.GetFloat("volume");
    }

    public void PlayButton()
    {
        SceneManager.LoadScene("MainScene");
    }
    public void ScoreButton()
    {
        throw new NotImplementedException();
        //SceneManager.LoadScene("ScoreMenu");
    }
    public void OptionButton()
    {
        SceneManager.LoadScene("OptionMenu");
    }
}
