using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchButton : MonoBehaviour
{
    public Image firstOption;
    public Image secondOption;
    public Sprite firstOptionOff;
    public Sprite firstOptionOn;
    public Sprite secondOptionOff;
    public Sprite secondOptionOn;
    private bool firstSelected=false;
    // Update is called once per frame
    void Update()
    {
        if (firstSelected) {
            firstOption.sprite = firstOptionOn;
            secondOption.sprite = secondOptionOff;
        }
        else
        {
            firstOption.sprite = firstOptionOff;
            secondOption.sprite = secondOptionOn;
        }
    }
    protected virtual void setOn()
    {
        firstSelected = false;
    }
    protected virtual void setOff()
    {
        firstSelected = true;
    }

    //accessor ( i.e. a get method )
    public bool FirstSelected
    {
        get
        {
            return firstSelected;
        }
        set
        {
            firstSelected = value;
        }
    }

}
