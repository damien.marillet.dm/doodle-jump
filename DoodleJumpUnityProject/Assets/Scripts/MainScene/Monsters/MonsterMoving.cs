using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMoving : Monster
{
    private Camera cam;
    private float screenWidth;
    private float speed;
    private float direction;
    private Vector3 monsterSize;

    private void Start()
    {
        speed = 0.02f;
        System.Random rnd = new System.Random();
        cam = Camera.main.GetComponent<Camera>();
        monsterSize = GetComponent<Renderer>().bounds.size;
        screenWidth = cam.orthographicSize * Screen.width / Screen.height - monsterSize.x/2;
        impulseMagnitude = 10f;
        if (rnd.Next(2) == 0)
        { 
            direction = 1f;
        } 
        else 
        {
            direction = -1f;
        }
    }

    private void FixedUpdate()
    {
        if (transform.position.x > screenWidth ^ transform.position.x < -screenWidth)
        {
            direction = -direction;
        }
        transform.Translate(new Vector2(speed*direction,0f));
    }
}
