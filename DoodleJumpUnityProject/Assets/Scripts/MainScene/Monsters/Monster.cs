using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : ObjectToJumpOn
{
    void Start()
    {
        impulseMagnitude = 10f;
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject player = collision.gameObject;
        if (player.GetComponent<BoxCollider2D>().enabled){
            Rigidbody2D playerRb = player.GetComponent<Rigidbody2D>();
            if (collision.relativeVelocity.y < 0)
            {
                playerRb.AddForce(new Vector2(playerRb.velocity.x, impulseMagnitude), ForceMode2D.Impulse);
                gameObject.SetActive(false);
            } else {
                print("GAME OVER MONSTER");
                player.GetComponent<BoxCollider2D>().enabled = false;
            }
        } else {
            // print("Monster colli disabled");
        }
        
    }
}
