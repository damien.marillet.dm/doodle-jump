using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private Rigidbody2D playerRb;
    private float playerSpeed = 7f;
    private float moveX;
    private Animator animator;
    public bool isDead = false;
    public bool isTrapped = false;
    public static bool holdingObject = false;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        playerRb = GetComponent<Rigidbody2D>();
        GetComponent<BoxCollider2D>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        // print("player"+gameObject.GetComponent<BoxCollider2D>().enabled);
        if (!isTrapped){
            moveX = Input.GetAxis("Horizontal") * playerSpeed;
            if(moveX!=0){
                animator.SetBool("isFacingLeft", Mathf.Sign(moveX) < 0 ? true : false);
            }
        }
    }

    private void FixedUpdate()
    {
        if (!isTrapped){
            Vector2 velocity = playerRb.velocity;
            velocity.x = moveX;
            playerRb.velocity = velocity;
        }
    }

    public bool getHoldingObject(){
        return holdingObject;
    }

    public void setHoldingObject(bool boolean){
        holdingObject = boolean;
    }

    public void Trap(){
        isTrapped = true;
    }

    // public bool getEnabled(bool boolean){
    //     return gameObject.GetComponent<BoxCollider2D>().enabled;
    // }

    // public void setEnabled(bool boolean){
    //     gameObject.GetComponent<BoxCollider2D>().enabled = boolean;
    // }
}
