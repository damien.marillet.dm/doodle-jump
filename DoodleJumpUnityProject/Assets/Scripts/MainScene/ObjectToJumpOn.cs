using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectToJumpOn : MonoBehaviour
{
    protected float impulseMagnitude;

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject colli = collision.gameObject;
        Rigidbody2D playerRb = colli.GetComponent<Rigidbody2D>();
        Animator playerAnimator = colli.GetComponent<Animator>();
        if (collision.relativeVelocity.y < 0)
        {
            playerRb.AddForce(new Vector2(playerRb.velocity.x, impulseMagnitude), ForceMode2D.Impulse);

            //play jump animation
            bool isFacingLeft = playerAnimator.GetBool("isFacingLeft");
            playerAnimator.Play("Jump" + (isFacingLeft ? "Left" : "Right"));
        }
    }
}
