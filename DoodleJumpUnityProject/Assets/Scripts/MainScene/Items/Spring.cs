using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spring : ObjectToJumpOn
{
    private AudioSource audioSource;
    void Start()
    {
        impulseMagnitude = 20f;
        audioSource = GetComponent<AudioSource>();
    }
    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (collision.relativeVelocity.y < 0)
        {
            audioSource.Play();
        }
    }
}
