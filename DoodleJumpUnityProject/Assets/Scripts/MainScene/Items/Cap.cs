using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cap : ObjectToHold
{
    private AudioSource audioSource;

    void Start(){
        audioSource = GetComponent<AudioSource>();
        timeEffect = 2.5f;
        // timeEffect = 10f;
        capSpeed = 12f;
        effectIsActive = false;
        position = new Vector2(0f, playerRender.GetComponent<Renderer>().bounds.size.y/3);
    }
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        audioSource.Play();
    }
}
