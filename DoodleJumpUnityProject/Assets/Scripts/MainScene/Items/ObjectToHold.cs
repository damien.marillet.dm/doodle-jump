using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class ObjectToHold : MonoBehaviour
{   

    protected float timeEffect;
    protected float capSpeed;
    private float startEffect;
    private Rigidbody2D playerRb;
    protected bool effectIsActive;
    public Renderer playerRender;
    protected GameObject playerColli;
    protected Vector2 position;

    void Start(){
        effectIsActive = false;
    }

    void Update()
    {
        if (effectIsActive) {
            if (startEffect + timeEffect > Time.time ){
                playerRb.velocity = new Vector2(playerRb.velocity.x, capSpeed);
                Vector3 newPosition = new Vector3(playerColli.transform.position.x + position.x, playerColli.transform.position.y + position.y, playerColli.transform.position.z); 
                transform.position = newPosition;
            }
            else {
                effectIsActive = false;
                playerColli.GetComponent<Player>().setHoldingObject(false);
                playerColli.GetComponent<BoxCollider2D>().enabled = true;
                gameObject.SetActive(false);
            }
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        // test if the collider is the player
        startEffect = Time.time;
        playerColli = collision.gameObject;
        playerRb = collision.attachedRigidbody;
        effectIsActive = true;
        playerColli.GetComponent<BoxCollider2D>().enabled = false;
        playerColli.GetComponent<Player>().setHoldingObject(true);
    }
}
