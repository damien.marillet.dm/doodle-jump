using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class greenPlatform : ObjectToJumpOn
{

    private AudioSource audioSource;
    void Start()
    {
        impulseMagnitude = 10f;
        audioSource = GetComponent<AudioSource>();
    }
    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (collision.gameObject.name == "Player" && collision.relativeVelocity.y<0)
        {
            audioSource.Play();
        }
    }
}
