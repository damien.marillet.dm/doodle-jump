using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bluePlatform : ObjectToJumpOn
{
    private Camera cam;
    private float screenWidth;
    private float speed;
    private float direction;
    private Vector3 platformsSize;
    private AudioSource audioSource;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        impulseMagnitude = 10f;
        speed = 0.01f;
        System.Random rnd = new System.Random();
        cam = Camera.main.GetComponent<Camera>();
        platformsSize = GetComponent<Renderer>().bounds.size;
        screenWidth = cam.orthographicSize * Screen.width / Screen.height - platformsSize.x/2;
        if (rnd.Next(2) == 0)
        { 
            direction = 1f;
        } 
        else 
        {
            direction = -1f;
        }
    }

    private void FixedUpdate()
    {
        if (transform.position.x > screenWidth ^ transform.position.x < -screenWidth)
        {
            direction = -direction;
        }
        transform.Translate(new Vector2(speed*direction,0f));
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (collision.gameObject.name == "Player" && collision.relativeVelocity.y < 0)
        {
            audioSource.Play();
        }
    }
}
