using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class brownPlatform : MonoBehaviour
{
    private bool launchFading = false;
    private Animator animator;
    private AudioSource audioSource;
    private void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }
    private void Update()
    {
        if (launchFading)
        {
            StartCoroutine("FadingAway");
            launchFading = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name=="Player" && collision.attachedRigidbody.velocity.y < 0 && launchFading == false)
        {
            launchFading = true;
            animator.SetBool("isJumpedOn",true);
            audioSource.Play();
            gameObject.GetComponent<EdgeCollider2D>().enabled = false;
        }
    }
    private IEnumerator FadingAway()
    {
        // print("lauching coroutine");
        int cpt = 0;
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        while (renderer.isVisible)
        {
            // print(cpt++);
            cpt++;
            Vector3 p =transform.position;
            p.y -= 5f * Time.deltaTime;
            transform.position = p;
            //supposedly should decrease sprite transparency with a 30% rate every second
            Color c = renderer.color;
            c.a *= (1-0.3f*Time.deltaTime);
            renderer.color = c;
            yield return null;
        }
            
    }
    void OnBecameInvisible()
    {
        gameObject.SetActive(false);
        //Destroy(this.gameObject);
    }
}
