using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//IMPORTANT : script a attacher au joueur
public class ToroidalEnvironment : MonoBehaviour
{
    public Camera mainCam;

    // Start is called before the first frame update
    void Start()
    {
        //Camera mainCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 screenPos = mainCam.WorldToScreenPoint(transform.position);
        float fromLeft = screenPos.x;
        float fromRight = mainCam.pixelWidth - screenPos.x;
        float tolerance = 20;

        if (fromLeft < -tolerance)
        {
            float newX = mainCam.ScreenToWorldPoint(new Vector3(mainCam.pixelWidth, 0)).x;
            //update position
            transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        }else if ( fromRight< -tolerance)
        {
            float newX = mainCam.ScreenToWorldPoint(Vector3.zero).x;
            //update position
            transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        }

    }
}
