using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    private GameObject player;
    private Rigidbody2D playerRb;
    private bool isTrapped = false;
    public GameManager gameManager;
    public float coeff = 1f;
    public float coeffRot=90f;
    public float coeffShrink=1f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        player = collision.gameObject;
        playerRb = player.GetComponent<Rigidbody2D>();
        if (!isTrapped)
        {
            playerRb.velocity = Vector2.zero;
            playerRb.gravityScale = 0;

            isTrapped = true;
            player.GetComponent<Player>().Trap();
            StartCoroutine(Fade());
        }

    }

    private IEnumerator Fade()
    {
        while (true)
        {
            player.transform.Rotate(0, 0, coeffRot * Time.deltaTime);
            player.transform.localScale*=0.99f;

            Vector2 fromPlayerToBH = this.transform.position-player.transform.position;
            print(playerRb.velocity);
            playerRb.velocity = fromPlayerToBH * coeff; // to absorb the player in timeAnim seconds

            if (fromPlayerToBH.sqrMagnitude < 0.1)
            {
                gameManager.isGameOver = true;
            }
            yield return null;
        }
    }

}
