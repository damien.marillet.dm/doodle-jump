using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    public Transform target;
    public GameManager gameManager;

    private void LateUpdate()
    {
        if (target.position.y > transform.position.y)
        {
            Vector3 newPosition = new Vector3(transform.position.x, target.position.y, transform.position.z); 
            transform.position = newPosition;
        }

        if (target.position.y < transform.position.y-Camera.main.GetComponent<Camera>().orthographicSize)
        {
            gameManager.isGameOver = true;
        }

        if (gameManager.isGameOver)
        {
            float clampedY = Mathf.Max(target.position.y, -10);
            Vector3 newPosition = new Vector3(transform.position.x, clampedY, transform.position.z);
            transform.position = newPosition;
        }
    }
}
